﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateShoot : MonoBehaviour
{
    [SerializeField]
    GameObject prefabShoot;
    [SerializeField]
    GameObject prefabMissile;
    [SerializeField]
    float fireRate;
    [SerializeField]
    float missileCooldown;
    [SerializeField]
    float shootPoint;

    bool turns = false;

    void Start()
    {
        StartCoroutine(RatingFire());
        StartCoroutine(Missile());
    }

    IEnumerator RatingFire()
    {
        yield return new WaitForSeconds(fireRate);
        Vector3 shotSpawnPoint = transform.position;

        if (turns)
        {
            turns = false;
            shotSpawnPoint.x += shootPoint;
        }
        else if (!turns)
        {
            turns = true;
            shotSpawnPoint.x -= shootPoint;
        }

        Instantiate(prefabShoot, shotSpawnPoint, Quaternion.identity);
        StartCoroutine(RatingFire());
    }

    IEnumerator Missile()
    {
        yield return new WaitForSeconds(missileCooldown);

        Instantiate(prefabMissile, transform.position, Quaternion.identity);
        StartCoroutine(Missile());
    }
}
