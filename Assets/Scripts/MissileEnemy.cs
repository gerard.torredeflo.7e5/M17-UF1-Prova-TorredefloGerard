﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileEnemy : MonoBehaviour
{
    [SerializeField]
    float missileSpeed;
    void Update()
    {
        float actualSpeed = missileSpeed / 20;
        Vector3 goStraight = transform.position;
        goStraight.y -= actualSpeed;
        transform.position = goStraight;
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
