﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateShotScript : MonoBehaviour
{
    [SerializeField]
    float shotSpeed;
    void Update()
    {
        float actualSpeed = shotSpeed / 15;
        Vector3 goStraight = transform.position;
        goStraight.y -= actualSpeed;
        transform.position = goStraight;
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
