﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStatus : MonoBehaviour
{
    bool playerKill;
    bool isAlive = true;

    void Update()
    {
        if (!isAlive)
        {
            if (playerKill)
            {
                GameManager.Instance.defeatedEnemies += 5;
            }
            Destroy(gameObject);
        }

        if (transform.position.y <= -5.4)
        {
            playerKill = false;
            isAlive = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerKill = true;
            isAlive = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Missile")
        {
            playerKill = true;
            isAlive = false;
        }
    }
}
