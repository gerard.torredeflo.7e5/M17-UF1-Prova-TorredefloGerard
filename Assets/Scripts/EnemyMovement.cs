﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField]
    float speed;

    void Update()
    {
        Vector3 heGoesDown = transform.position;
        float realSpeed = speed / 50;
        heGoesDown.y -= realSpeed;
        transform.position = heGoesDown;
    }
}
