﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileScript : MonoBehaviour
{
    [SerializeField]
    float missileSpeed;
    void Update()
    {
        float actualSpeed = missileSpeed / 2;
        Vector3 goStraight = transform.position;
        goStraight.y += actualSpeed;
        transform.position = goStraight;
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
