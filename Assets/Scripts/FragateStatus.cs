﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateStatus : MonoBehaviour
{
    [SerializeField]
    int lifes;
    bool playerKill;

    void Update()
    {
        if (lifes <= 0)
        {
            if (playerKill)
            {
                GameManager.Instance.defeatedEnemies += 20;
            }
            Destroy(gameObject);
        }

        if (transform.position.y <= -7.5)
        {
            playerKill = false;
            lifes = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Missile")
        {
            playerKill = true;
            lifes -= 1;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerKill = true;
            lifes -= 1;
        }
    }
}
