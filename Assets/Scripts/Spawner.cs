﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    GameObject prefabEnemy;
    [SerializeField]
    GameObject prefabFragate;

    float minX = -7.6f;
    float maxX = 7.6f;
    float spawnPoint;
    [SerializeField]
    float enemySpawnTime;
    [SerializeField]
    float fragateSpawnTime;


    private void Start()
    {
        StartCoroutine(SpawnFragate());
        StartCoroutine(SpawnEnemy());
    }

    IEnumerator SpawnEnemy()
    {
        spawnPoint = Random.Range(minX, maxX);
        yield return new WaitForSeconds(enemySpawnTime);
        Vector3 spawn = transform.position;
        spawn.x = spawnPoint;
        Instantiate(prefabEnemy, spawn, Quaternion.identity);
        StartCoroutine(SpawnEnemy());
    }

    IEnumerator SpawnFragate()
    {
        spawnPoint = Random.Range(minX, maxX);
        yield return new WaitForSeconds(fragateSpawnTime);
        Vector3 spawn = transform.position;
        spawn.x = spawnPoint;
        Instantiate(prefabFragate, spawn, Quaternion.identity);
        StartCoroutine(SpawnFragate());
    }
}
