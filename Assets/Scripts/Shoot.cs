﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField]
    GameObject prefabMissile;
    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            Instantiate(prefabMissile, transform.position, Quaternion.identity);
        }
    }
}
